class Animal{
    constructor(name,age,spec){
        this.name = name;
        this.age = age;
        this.spec = spec;
    }
    speak(){
        return "mew mew"
    }
    get getAge(){
        return this.age;
    }
    static sumCatAge(cat1,cat2){
        return cat1.age + cat2.age;
    }
}

let cat1 = new Animal("paw",2,"cat");
let cat2 = new Animal ("paw1",5,"cat")
console.log(cat1);
//check cat
cat1 instanceof Animal
console.log(cat1.speak());
//get khong truyen vaof tham so dc
//static de xu li cac instance voi nhau
console.log(Animal.sumCatAge(cat1,cat2));


