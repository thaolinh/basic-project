let cat = {tiredness:50,
         hunger:50, 
         lonliness:50, 
         happiness:50, 
         feed(){
             this.hunger -= 10;
             this.happiness +=10;
         },
         sleep(){
             this.tiredness -= 10;
             this.happiness += 10;
         },
         pet(){
             this.lonliness +=10;
         },
         status(){
             let str = ''
             this.tiredness>50 ? str+='Paw is very tired' : str+='';
             this.hunger>50 ? str+='Paw is very hungry' : str+='';
             this.lonliness>50 ? str+='Paw is very lonely' : str+='';
             this.happiness>50 ? str+='Paw is very happy' : str+=''; 
         }
}
